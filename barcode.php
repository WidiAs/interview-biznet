<?php
include "phpqrcode/qrlib.php";

$isi = 'test';
$jenis = 'qr';

if ($jenis == 'barcode'){
    echo '<img src="php-barcode-master/barcode.php?text='.$isi.'&print=true&size=45" />';
}elseif ($jenis == 'qr'){
    $tempdir="qr/";
    if (!file_exists($tempdir))   mkdir($tempdir, 0755);
    $file_name=$isi.".png";
    $file_path = $tempdir.$file_name;
    QRcode::png($isi, $file_path, "H", 6, 4);
    echo "<p><img src='".$file_path."' /></p>";
}

?>